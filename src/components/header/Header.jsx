import './header.css';

export const Header = ({ messages }) => {
    function countUsers() {
        let amountlist = [];
        for (let message of messages) {
            if (amountlist.includes(message.user)) {
                continue;
            } else {
                amountlist.push(message.user)
            }
        }
        return amountlist.length
    }
    let messagesArray = Array.from(messages);
    messagesArray = messagesArray.sort(function (a, b) {
        return new Date(b.createdAt) - new Date(a.createdAt);
    });
    let lastMessage = new Date(messagesArray[0].createdAt);
    return (
        <header className="header">

            <div className="header-title header-block">Random Chat</div>
            <div className="header-users-count header-block">{countUsers() + 1}</div>

            <div className="header-messages-count header-block">{messages.length}</div>
            <div className="header-last-message-date header-block">Last message at <span className='header-last-message-date__span'>{new Intl.DateTimeFormat("en-GB", {
                day: '2-digit',
                month: 'numeric',
                year: 'numeric',
                hour: 'numeric',
                minute: 'numeric'
            }).format(lastMessage)}</span></div>
        </header>)
};
