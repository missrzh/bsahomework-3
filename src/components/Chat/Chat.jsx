import { Header } from "../header/Header.jsx"
import { MessageInput } from "../messages/MessageInput.jsx"
import { MessageList } from "../messages/MessageList.jsx"
import React from "react";
import './chat.css';
import { Preloader } from "../preloader/Preloader.jsx";

class Chat extends React.Component {
    constructor(props) {
        super(props)
        this.url = props.url
        this.state = {
            messages: [],
            isLoading: true,
        };

    }
    deleteMessageHandler(id) {
        const oldMessages = this.state.messages;
        const newMessages = oldMessages.filter((message) => message.id !== id)
        this.setState({ messages: newMessages })
    }
    createMessageHandler(text) {
        const date = new Date().toJSON();
        const id = Math.random().toString(16).slice(2);

        const oldMessages = this.state.messages;
        const newMessage = {
            id: id,
            userId: "9e243930-83c9-11e9-8e0c-8f1a68612345",
            avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
            user: "Misha",
            text: text,
            createdAt: date,
            editedAt: ""
        }
        this.setState({ messages: [...oldMessages, newMessage] })
        console.log(this.state.messages)
    }
    componentDidMount() {
        this.setState({ isLoading: true });
        fetch(this.url)
            .then(response => response.json())
            .then((data) => { this.setState({ messages: data, isLoading: false }) })

    }


    render() {
        return (this.state.isLoading ? <><Preloader /></> :
            <div className="main" >
                <Header messages={this.state.messages} />
                <MessageList messages={this.state.messages} deleteMessageHandler={this.deleteMessageHandler.bind(this)} />
                <MessageInput createMessageHandler={this.createMessageHandler.bind(this)} />
            </div>

        );
    }
};

export default Chat;