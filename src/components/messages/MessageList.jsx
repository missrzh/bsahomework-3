import { Message } from "./messagesBlocks/Message";
import { OwnMessage } from "./messagesBlocks/OwnMessage"
import './messages.css';

import React from "react";

export const MessageList = ({ messages, deleteMessageHandler }) => {


    function changePrev(message) {
        previousDay = new Date(message.createdAt).getDay()
    }

    let messagesArray = Array.from(messages);
    messagesArray = messagesArray.sort(function (a, b) {
        return new Date(b.createdAt) - new Date(a.createdAt);
    }).reverse();
    let previousDay = 0;
    return (
        <div className="message-list">
            {Array.from(messagesArray, (message, ids) => (

                <div key={ids}>
                    {new Date(message.createdAt).getDay() !== previousDay ? <div className="divider__wrapper">
                        <div className="messages-divider">{new Intl.DateTimeFormat("en-GB", {
                            weekday: 'long',
                            day: '2-digit',
                            month: 'long'
                        }).format(new Date(message.createdAt))}</div>
                    </div> : <></>}
                    {changePrev(message)}
                    {message.userId === '9e243930-83c9-11e9-8e0c-8f1a68612345' ? <div className="own-message__wrapper">
                        <OwnMessage message={message} deleteMessageHandler={deleteMessageHandler} />
                    </div> : <div className="message__wrapper" >
                        <Message message={message} />
                    </div>}


                </div>

            ))}


        </div>)

};