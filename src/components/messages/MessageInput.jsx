import './messages.css'
import { useState } from 'react';

export const MessageInput = ({ createMessageHandler }) => {
    const handleSendMessage = () => { createMessageHandler(message) }
    const [message, setMessage] = useState("");
    const handleInput = (event) => { setMessage(event.target.value) }
    return (
        <div className="message-input">
            <input className="message-input-text" onChange={handleInput} value={message} />
            <button className="message-input-button" onClick={handleSendMessage}>Send</button>
        </div>)
};