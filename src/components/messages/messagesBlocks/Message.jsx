import './messageText.css';
import React from "react";

export class Message extends React.Component {
    constructor(props) {
        super(props)
        this.message = props.message;
        this.state = {
            liked: false,
        }
    }

    render() {
        let timemy = new Date(this.message.createdAt);
        let edited = this.message.editedAt === "" ? "" : new Date(this.message.editedAt);
        return (
            <div className="message">
                <div className='info__wrapper'>
                    <div className="image-cropper">
                        <img className="message-user-avatar" src={this.message.avatar} alt='avatar' />
                    </div>
                    <div className="message-user-name">{this.message.user}</div>
                </div>
                <div className="message-text">{this.message.text}</div>
                <div className='time__wrapper'>
                    {this.state.liked ?
                        <button className='message-liked' onClick={() => this.setState({ liked: false })}>Like</button> :
                        <button className='message-like' onClick={() => this.setState({ liked: true })}>Like</button>}

                    <div className="message-time">{this.message.editedAt === "" ? new Intl.DateTimeFormat("en-GB", {
                        hour: 'numeric',
                        minute: 'numeric'
                    }).format(timemy) : 'edited ' + new Intl.DateTimeFormat("en-GB", {
                        hour: 'numeric',
                        minute: 'numeric'
                    }).format(edited)}</div>
                </div>
            </div>)
    }
};