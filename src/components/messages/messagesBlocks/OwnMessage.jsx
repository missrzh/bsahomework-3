

export const OwnMessage = ({ message, deleteMessageHandler }) => {
    let timemy = new Date(message.createdAt);
    let edited = message.editedAt === "" ? "" : new Date(message.editedAt);
    const handleDelete = () => deleteMessageHandler(message.id);
    return (
        <div className="own-message">
            <div className='time__wrapper'>
                <div className="message-time">{message.editedAt === "" ? new Intl.DateTimeFormat("en-GB", {
                    hour: 'numeric',
                    minute: 'numeric'
                }).format(timemy) : 'edited ' + new Intl.DateTimeFormat("en-GB", {
                    hour: 'numeric',
                    minute: 'numeric'
                }).format(edited)}</div>
            </div>
            <div className="message-text">{message.text}</div>

            <button className="message-edit">edit</button>
            <button className="message-delete" onClick={handleDelete}>delete</button>
        </div>)
};